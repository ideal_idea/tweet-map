#### Resources

0. [Tweets | Twitter Developers](https://dev.twitter.com/overview/api/tweets)

1. [Tweepy 3.5.0 documentation](http://tweepy.readthedocs.org/en/v3.5.0/getting_started.html)

2. [SQS — Boto 3 Docs 1.3.0 documentation](http://boto3.readthedocs.org/en/latest/reference/services/sqs.html#SQS.Queue.send_message)
    - [SQS.Queue.receive_messages](http://boto3.readthedocs.org/en/latest/reference/services/sqs.html#SQS.Queue.receive_messages)

2. ElasticSearch
  - [Having Fun: Python and Elasticsearch, Part 1 - bitquabit](https://bitquabit.com/post/having-fun-python-and-elasticsearch-part-1/)
  - [Bulk API](https://www.elastic.co/guide/en/elasticsearch/reference/1.5/docs-bulk.html)
  `es.indices.delete(index = opt.index)`

10. [Creating a singleton in Python - Stack Overflow](http://stackoverflow.com/questions/6760685)


#### todo

1. extract/learn necessary code from tweepy

2. geo is obsolete



